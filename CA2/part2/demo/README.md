# Relatório do Exercício Prático CA2 - parte 2 

## 1. Análise, Design e Implementação

O respetivo exercício contempla o uso do repositório criado por cada aluno, nesse exercício foi utilizado o repositório do bitbucket criado com o nome devops-19-20-b<1181555>, 
foi copiado para a pasta CA2/part2 os ficheiros necessários a partir da pasta basic presente na pasta CA1 que foi anteriormente retirada do tutorial, assim como os ficheiros de um 
projecto gradle spring com as dependências Rest Repositories, Thymeleaf, JPA, H2. Esse ficheiros foram retirados do inicializador spring que está no site https://start.spring.io.
O readme desse exercício encontra-se disponível no respetivo link [CA2](https://bitbucket.org/1181555/devops-19-20-b/src/master/CA2/part2/demo/README.md). 

### Adicionando Gradle Plugin

Foi adicionado um plugin "org.siouan.frontend" para ser possível o Gradlew gerenciar a parte do frontend, então no ficheiro build.gradlew
adicionou-se as seguintes dependências:

```gradlew
plugins {
    id "org.siouan.frontend" version "1.4.1"
}
```
```gradlew
frontend {
    nodeVersion = "12.13.1"
    assembleScript = "run webpack"
}
```
Atualizou-se também a seção do script em package.json para configurar a execução do pacote web. 
```gradlew
"scripts": {
    "watch": "webpack --watch -d",
    "webpack": "webpack"
  }
```

Posteriormente foi executado o comando para que as tarefas relacionadas com o fronted fossem executadas e o código do fronted fosse gerado.
```gradlew
gradlew build
```

## Criação da tarefa de *cópia* no Gradle 

A criação da tarefa de cópia para gerar ficheiro jar numa pasta chamada "dist" localizada no nível da pasta raiz do projecto foi feita com o respetivo código: 

```gradlew
task copyTask(type: Copy) {
    from 'build/libs'
    into 'dist'
}
```

## Criação da tarefa *delete* no Gradle 

A criação da tarefa "delete" para apagar todos os ficheiros gerados pelo pacote web com dependência do comando clean, sendo a tarefa "delete" executada antes da tarefa clean 
foi realizada com o seguinte código:

```gradlew
task makeDelete(type: Delete) {
    dependsOn clean
    clean.doFirst {
        project.delete '/src/main/resources/static/built/'
    }
}
```

## 2. Análise do alternativa ao Gradle

O Apache Ant é uma biblioteca Java usada para automatizar processos de construção para aplicativos Java. 
Além disso, o Ant pode ser usado para construção de aplicações não Java. Inicialmente, fazia parte da base de código do Apache Tomcat e foi lançado como um projeto independente em 2000.

Em muitos aspectos, o Ant é muito semelhante ao Make, e é simples o suficiente para que qualquer pessoa possa começar a usá-lo sem nenhum pré-requisito específico. Os arquivos de compilação do Ant são gravados em XML e, por convenção, são chamados de build.xml.
Diferentes fases de um processo de construção são chamadas de "targets".

Esse ficheiro "build" define quatro "target": "clean", "compile", "jar" e "run". 
Por exemplo, podemos compilar o código executando:

```Ant
ant compile
```

Esse comando acionará a limpeza do destino primeiro, o que excluirá o diretório "classes". Depois disso, a compilação de destino recriará o diretório e compilará a pasta "src" nele.
O principal benefício do Ant é a sua flexibilidade. O Ant não impõe convenções de codificação ou estruturas de projeto. Consequentemente, isso significa que o Ant exige que os desenvolvedores escrevam todos os comandos por si mesmos, o que às vezes leva a enormes arquivos de construção XML difíceis de manter.

Como não existem convenções, apenas conhecer o Ant não significa que entenderemos rapidamente qualquer arquivo de construção do Ant. Provavelmente levará algum tempo para se acostumar com um arquivo Ant desconhecido, o que é uma desvantagem em comparação com outras ferramentas mais recentes.

No início, o Ant não tinha suporte interno para gerenciamento de dependências. No entanto, como o gerenciamento de dependências se tornou obrigatório nos últimos anos, o Apache Ivy foi desenvolvido como um subprojeto do projeto Apache Ant. Está integrado ao Apache Ant e segue os mesmos princípios de design.
No entanto, as limitações iniciais do Ant devido à falta de suporte interno para gerenciamento de dependências e frustrações ao trabalhar com arquivos de construção XML incontroláveis ​​levaram à criação do Maven.


## 3. Implementação da Alternativa - *Ant*

A implementação alterantiva foi realizada com o Apache Ant. Como visto na seção anterior o Apache Ant é usado para automatizar processos de construção, assim como o Gradle.
No ficheiro build.xml do Ant foi escrito as respetivas tarefas de cópia e no caso do "delete" quando fosse executado o comando clean apagar os ficheiros da pasta built.

##### Tarefa de cópia
```gradlew
 <target name="taskCopy">
    <copy todir="dist"
        <fileset dir="build/libs"/>
    </copy>
 </target>
```

##### Tarefa delete
```gradlew
 <target name="taskDelete">
    <delete dir="src/main/resources/static/built">
    </delete>
 </target>
 <target name="clean" depends="taskDelete" description="Delete the build directory">
    <delete dir="build"/>
 </target>
```