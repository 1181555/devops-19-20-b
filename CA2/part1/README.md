# Relatório do Exercício Prático CA2 - parte 1 

## 1. Análise, Design e Implementação

O respetivo exercício contempla o uso do repositório criado por cada aluno, nesse exercício foi utilizado o repositório do bitbucket criado com o nome devops-19-20-b<1181555>, 
foi copiado para a pasta CA2/part1 a partir do exemplo da aplicação disponível no [repositório](https://bitbucket.org/luisnogueira/gradle_basic_demo/) fornecido pelo docente da disciplina.
O readme desse exercício encontra-se disponível no respetivo link [CA2](https://bitbucket.org/1181555/devops-19-20-b/src/master/CA2/part1/README.md). 

### Instruções do arquivo README 

O primeiro passo do exercício consistiu em seguir as instruções disponibilizados pelo ficheiro README do exercício. 
Foram utilizados os seguintes comandos para construir o projecto, correr o servidor e o correr o cliente:

```gradlew
1. gradlew build
```
```gradlew
2. java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001
```
```gradlew
3. gradlew runClient
```

Os respetivos comandos permitiram a criação e execução de um chat.

### Criação da tarefa *runServer* no Gradle

Posteriormente, para criar a tarefa runServer no Gradle foi aberto o ficheiro build.gradle e definida a seguinte tarefa:

```gradlew
task runServer(type: JavaExec, dependsOn: classes) {
    classpath = sourceSets.main.runtimeClasspath
    main = 'basic_demo.ChatServerApp'
    args '59001'
}
```

### Adicionando um teste unitário e atualizando the gradle script

Foi criado o ficheiro AppTest na respetiva pasta de teste e com o seguinte código: 
```gradlew
pasta: src/test/java/basic_demo/AppTest.java

 @Test
    public void testAppHasAGreeting() {
        App classUnderTest = new App();
        assertNotNull("app should have a greeting", classUnderTest.getGreeting());
    }
```
Para que o teste pudesse correr adequadamente a quando da construção e execução dos testes foi adicionada as seguintes dependências no ficheiro build.gradle: 

```gradlew
 dependencies {
     testCompile 'junit:junit:4.12'
     testImplementation 'junit:junit:4.12'
     testRuntime 'junit:junit:4.12'
 }
```

## Criação das tarefas de *cópia* e *zip* no Gradle 

A criação das tarefa de cópia e zip foram definidas pelos seguintes comandos no ficheiro build.gradle:

```gradlew
task copyTask(type: Copy) {
    from 'src'
    into 'backup'
} 
```

```gradlew
task zipTask(type: Zip) {
    from '/src/'
    archiveName 'src.zip'
    destinationDir(file('/zip'))
}
```

#Anexo:

##Gradle Basic Demo


This is a demo application that implements a basic multithreaded chat room server.

The server supports several simultaneous clients through multithreading. When a client connects the server requests a screen name, and keeps requesting a name until a unique one is received. After a client submits a unique name, the server acknowledges it. Then all messages from that client will be broadcast to all other clients that have submitted a unique screen name. A simple "chat protocol" is used for managing a user's registration/leaving and message broadcast.


Prerequisites
-------------

 * Java JDK 8
 * Apache Log4J 2
 * Gradle 6.6 (if you do not use the gradle wrapper in the project)
   

Build
-----

To build a .jar file with the application:

    % ./gradlew build 

Run the server
--------------

Open a terminal and execute the following command from the project's root directory:

    % java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp <server port>

Substitute <server port> by a valid por number, e.g. 59001

Run a client
------------

Open another terminal and execute the following gradle task from the project's root directory:

    % ./gradlew runClient

The above task assumes the chat server's IP is "localhost" and its port is "59001". If you whish to use other parameters please edit the runClient task in the "build.gradle" file in the project's root directory.

To run several clients, you just need to open more terminals and repeat the invocation of the runClient gradle task