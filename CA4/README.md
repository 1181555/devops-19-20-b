# Relatório do Exercício Prático CA4

## 1. Análise, Design e Implementação

O respetivo exercício contempla o uso do repositório criado por cada aluno, nesse exercício foi utilizado o repositório do bitbucket criado com o nome devops-19-20-b<1181555>, 
foi desenvolvido o ficheiro README.md na pasta CA4 para descrever as etapas da tarefas desenvolvidas.
O readme desse exercício encontra-se disponível no respetivo link [CA4](https://bitbucket.org/1181555/devops-19-20-b/src/master/CA4/README.md). 

### Instalação e Configuração das máquinas virtuais

Nesse exercício foi feito o download do Docker Toolbox do URL https://docs.docker.com/toolbox/toolbox_install_windows/ e instalado no meu sistema operativo Windows.

- Foi feita uma instalação personalizada, desmarcando a opção de instalar o Virtualbox, pois tal instalação já tinha sido feito anteriormente. 

- Criou-se uma pasta CA4 no meu repositório para fazer o download dos arquivos que estão localizados no link https://bitbucket.org/atb/docker-compose-spring-tut-demo/src/master/

### Configuração dos Dockerfile e Docker-Compose

No ficheiro Dockerfile web foram feitos os seguintes ajustes para fazer o clone do meu repositório do bitbuck, definir o diretório de trabalho e dar permissões de execução ao gradle:  

```docker
RUN git clone https://1181555@bitbucket.org/1181555/devops-19-20-b.git

WORKDIR /tmp/build/devops-19-20-b/CA3/part2/demo

RUN chmod u+x ./gradlew
```

Em relação do Dockerfile db e docker-compose.yml não foram feitas alterações.

### Construção dos contentores

Para a construção dos contentores foi executado na linha de comando do Docker Quickstart Terminal, na respetiva pasta onde encontra-se o ficheiro docker-compose.yml a seguinte instrução:

```dokcer
docker-compose build
```
Após a construção dos contentores foi executado o seguinte comandos para iniciar os contentores:

```docker
docker-compose up
```

Depois de iniciado os contentores tentou-se aceder a aplicação web e db através do browser nos seguintes endereços:

```docker
http://localhost:8080/basic-0.0.1-SNAPSHOT/

http://localhost:8082/
```

Porém a página não foi encontrada devido ao facto de o docker toolbox usar o Virtualbox, ou seja, o localhost está em outro sistema. Então foi executado
o respetivos comando no Docker Quickstart Terminal para saber qual o endereço ip da máquina docker:

```docker
docker-machine ip default
```

Verificou-se que o endereço ip era 192.168.99.103 e então com os respetivos endereços foi possível aceder a aplicação web e db:

```docker
http://192.168.99.103:8080/basic-0.0.1-SNAPSHOT/

http://192.168.99.103:8082/
```

Por fim tentou-se aceder ao db para fazer o backup do ficheiro da base de dados com o comando:

```docker
docker-compose exec db /bin/bash
```
Porém devido ao facto de estar a usar o docker toolbox e a Virtualbox o comando gerou um erro e não foi possível aceder ao contentor db, em alternativa foi executado os seguintes comandos, primeiro
para listar os contentores e o segundo para aceder ao contentor:
```docker
docker ps

docker exec -it <contentor_id> /bin/bash
```

Foi feita a cópia da base de dados para a pasta '/usr/src/data' do contentor, porém não foi possível veficar que o backup estava disponível no disco local, pois meu projecto estava 
armazenado num disco diferente da máquina virtual. Posteriormente foi feito um teste colocando o projecto na pasta c:/users e foi possível ver que o backup da db encontava-se disponível

###Publicação das imagens dos meus contentores no Docker Hub

Primeiramente, foi feito um repositório público no Docker Hub, posteriormente na linha de comando do Docker Quickstart Terminal foi executado o seguinte comando para fazer o login no repositório do Docker Hub:

```docker
docker login --username=1181555
```
Com os seguintes comandos foi possível ver as imagens disponiveis, criar as tags para as imagens e fazer o push para o repositório:

```docker
docker images

docker tag fcdbe9b3d2e1 1181555/ca4_web:myappweb

docker tag 67b1dfdf4b33 1181555/ca4_db:myconsoledb

docker push 1181555/ca4_web

docker push 1181555/ca4_db
```

As imagens encontram-se disponiveis nos seguintes links:

```docker
https://hub.docker.com/repository/docker/1181555/ca4_web

https://hub.docker.com/repository/docker/1181555/ca4_db
```

## 2. Análise da alternativa ao Docker Toolbox

O Kubernetes é uma solução para orquestração de aplicações baseadas em containers. O Kubernetes utiliza uma solução de containers (como o Docker) para fazer o isolamento das aplicações especificadas nos serviços
ou pods que podem ser constituídos por um ou mais containers.

Com o Kubernetes, pode orquestrar um cluster de máquinas virtuais e agendar a execução de contentores nessas máquinas virtuais com base nos recursos de computação disponíveis e nos requisitos de recursos de cada contentor. 
Os contentores estão agrupados em pods, a unidade operacional básica do Kubernetes. Estes contentores e pods podem ser dimensionados para o estado pretendido e poderá gerir o respetivo ciclo de vida para manter as suas aplicações
operacionais.

Essencialmente, Kubernetes e Docker são duas tecnologias diferentes que funcionam bem em conjunto para a compilação, entrega e dimensionamento de aplicações contentorizadas.

Quando comparados, a comparação mais adequada é Kubernetes vs. Docker Swarm. O Docker Swarm é a tecnologia de orquestração da Docker que se dedica ao clustering para contentores do Docker e está totalmente integrado no ecossistema da
Docker e utiliza a sua própria API.

Uma diferença fundamental entre o Kubernetes e o Docker é que o primeiro destina-se a execução num cluster e o segundo num único nó. O Kubernetes é mais extenso do que o Docker Swarm e tem como objetivo coordenar clusters de nós em
escala em produção de forma eficiente. Os pods do Kubernetes, que são unidades de agendamento que contêm um ou mais contentores no ecossistema do Kubernetes, são distribuídos pelos nós para proporcionarem elevada disponibilidade.

Embora a promessa dos contentores seja programar uma vez e executar em qualquer local, o Kubernetes oferece o potencial de orquestrar e gerir todos os recursos de contentores num único plano de controlo. É útil em termos de rede, balanceamento de carga, segurança e
dimensionamento em todos os nós do Kubernetes que executam os contentores. O Kubernetes também tem um mecanismo de isolamento incorporado como os espaços de nomes que permitem agrupar recursos de contentores por permissão de acesso, ambientes de teste, entre outros critérios.
Estas construções permitem que a equipa de TI disponibilize mais facilmente acesso a recursos self-service a programadores e que estes colaborem até mesmo na mais complexa arquitetura de microsserviço sem terem de criar um protótipo de toda a aplicação no ambiente de desenvolvimento. Combinar as práticas de DevOps com contentores e Kubernetes origina ainda uma linha base de arquitetura de microsserviços que promove uma entrega rápida e uma orquestração dimensionável de aplicações nativas da cloud.

Resumidamente, utilize o Kubernetes com o Docker para:

Tornar a infraestrutura mais robusta e aumentar a disponibilidade da aplicação. A aplicação permanecerá online, mesmo que alguns nós fiquem offline.
Tornar a aplicação mais dimensionável. Se a aplicação começar a receber muito mais carga e precisar de aumentar horizontalmente para poder oferecer uma experiência de utilizador melhor, é fácil acelerar mais contentores ou adicionar mais nós ao cluster do Kubernetes.
O Kubernetes e o Docker trabalham em conjunto. O Docker fornece uma norma aberta para empacotar e distribuir aplicações em contentores. Através do Docker, pode criar e executar contentores, bem como armazenar e partilhar imagens de contentor. É fácil executar uma compilação do Docker num cluster do Kubernetes, o qual não é, por si só, uma solução completa. Para otimizar o Kubernetes na produção, aplique ferramentas e serviços adicionais para gerir a segurança, a governação, a identidade e o acesso, juntamente com os fluxos de trabalho de integração contínua/implementação contínua (CI/CD) e outras práticas de DevOps.

O Minikube é uma ferramenta que facilita executar o Kubernetes localmente, executando um cluster de Kubernetes com um único nó.
É muito bom para testar funcionalidades, aprender a trabalhar com o Kubernetes.

### 3. Implementação da Alternativa Minikube

Para a implementação da alternativa foi feito o download do Minikube do URL https://github.com/kubernetes/minikube/releases/tag/v1.10.1 e instalado no meu sistema operativo Windows.

Na linha de comandos foi executado o seguinte comando para iniciar o Minikube e criar um cluster:

```minikube
minikube start
```

Foi executado o respetivo comando para expartar as variáveis de ambiente Bash para configurar o ambiente local e assim reutilizar o daemon do Docker dentro da instância do 
Minikube.

```minikube
 minikube docker-env
```
```minikube
Output:
export DOCKER_TLS_VERIFY="1"
export DOCKER_HOST="tcp://192.168.99.100:2376"
export DOCKER_CERT_PATH="C:\Users\Tarcisio Ramalho\.minikube\certs"
export MINIKUBE_ACTIVE_DOCKERD="minikube"
```

Para apontar o meu shell para o docker-daemon do minikube, executou-se o comando:
```minikube 
eval $ (minikube -p minikube docker-env) 
```

Para fazer a construção o docker-compose a exemplo do exercício com docker foram executados os mesmos comandos:
```minikube 
docker-compose build

docker-compose up 
```

A partir dos seguintes endereços foi possível aceder a aplicação web e db:
  
```docker
http://192.168.99.100:8080/basic-0.0.1-SNAPSHOT/
  
http://192.168.99.100:8082/
```

#Anexo
# Docker Compose VM Demonstration

This project demonstrates how to run a container with Tomcat and other with H2.

## Requirements

Install Docker in you computer.

## How to build and execute

In the folder of the docker-compose.yml execute

  ```docker-compose build```

The execute

  ```docker-compose up```

### How to use

  In the host you can open the spring web application using the following url:

  - http://localhost:8080/basic-0.0.1-SNAPSHOT/

  You can also open the H2 console using the following url:

  - http://localhost:8080/basic-0.0.1-SNAPSHOT/h2-console

  For the connection string use: jdbc:h2:tcp://192.168.33.11:9092/./jpadb  
