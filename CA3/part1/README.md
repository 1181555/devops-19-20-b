# Relatório do Exercício Prático CA3 - parte 1 

## 1. Análise, Design e Implementação

O respetivo exercício contempla o uso do repositório criado por cada aluno, nesse exercício foi utilizado o repositório do bitbucket criado com o nome devops-19-20-b<1181555>, 
foi desenvolvido o ficheiro README.md na pasta CA3/part1 para descrever as etapas da tarefas desenvolvidas.
O readme desse exercício encontra-se disponível no respetivo link [CA3](https://bitbucket.org/1181555/devops-19-20-b/src/master/CA3/part1/README.md). 

### Configuração da máquina virtual

Esse exercício começou com a criação duma Virtual Machine usando o VirtualBox. Fou criado a Virtual Machine para suportar o sistema operativo do Linux, 
posteriormente foi instalado o sistema operativo Ubuntu 18.04. 
Na parte da configuração do Host Network Manager da VirtualBox, foi definido Host-only Adapter com o endereço IP 192.168.56.1/24.

Na instalação do network tools foi utilizado o seguinte comando:
```bash
sudo apt install net-tools
```
Após a execução do comando anterior foi possível editar o ficheiro de configuração de rede e ajustar o IP com o comando:
```bash
sudo nano /etc/netplan/01-netcfg.yaml
```
Foi aplicado a novas mudanças com o comando:
```bash
sudo netplan apply
```
Foi instalado o openssh-server e o ftp server com os seguintes comandos: 
```bash
sudo apt install openssh-server

sudo apt install vsftpd
```
Posteriormente, os servidos foram devidamente configurados.

### Instalação das *Aplicações* na máquina virtual 

Após a configuração da máquina virtual procedeu-se com a instalação do git, java, maven e gradle com os seguintes comandos: 
```bash
sudo apt install git

sudo apt install openjdk-8-jdk-headless

sudo apt install maven

sudo apt install gradle
```

Após a instalação dos softwares, foi configurado as variáveis de ambiente, e posteriormente foi feito o clone do repositório no Ubuntu com o comando: 
```bash
git clone https://1181555@bitbucket.org/1181555/devops-19-20-b.git
```

### Construção e execução do projectos do repositório 

####Execução do exercício CA1 
Na pasta do projecto, correspondente ao exercício CA1, foi executado o comando abaixo para iniciar o spring:boot:
```bash
mvn spring-boot:run
```
Como previamente havia sido instalado todos as aplicações necessárias para correr a simulação do servidor, com o Spring Boot, foi possível ver no Windows, com a utilização do
navegador Google Chrome no endereço previamente configurado http://192.168.56.10:8080/ que o servidor estava a correr normalmente.

####Execução do exercício CA2 part1
Na pasta do projecto, correspondente ao exercício CA2 part1, foi executado o comando abaixo para a construção da aplicação: 
```bash
gradle build
```
E posteriormente para a execução do servidor do chat foi utilizado o comando: 
```bash
gradle runServer
```
O servidor do chat começou a correr no Ubuntu e para verificar que era possível utilizar o chat, no ambiente Windows foi inicializado o cliente, isso se deve ao facto 
de o ambiente Ubuntu não ter suporte para parte gráfica.

No IDE Intellij, na tarefa 'RunClient' foi alterado o endereço IP para 192.168.56.10
```bash
task runClient(type:JavaExec, dependsOn: classes){
classpath = sourceSets.main.runtimeClasspath

main = 'basic_demo.ChatClientApp'

args '192.168.56.10', '59001'
}
```
E a parte da aplicação do chat referente ao cliente funcionou normalmente. Foi aberta duas janelas uma através do terminal do Intellij outra através do MS-DOS utilizando o comando:
```bash
gradle runClient
```

####Execução do exercício CA2 part2
Na pasta do projecto, correspondente ao exercício CA2 part2, foi executado o comando abaixo para a construção da aplicação: 
```bash
gradle build
```
Nesse caso, após o comando build, houve falha na construção devido a versão do Gradle 4 que tinha sido instalado anteriormente, então fiz a instalação 
do Gradle versão 6.3 e após a repetição do comando build a aplicação foi construída normalmente.

Posterimente foi executado o seguinte comando para correr a aplicação Spring Boot: 
```bash
gradle bootRun
```
Foi possível acessar a aplicação através do navegador utilizando o url http://192.168.56.10:8080/