# Relatório do Exercício Prático CA3 - parte 2 

## 1. Análise, Design e Implementação

O respetivo exercício contempla o uso do repositório criado por cada aluno, nesse exercício foi utilizado o repositório do bitbucket criado com o nome devops-19-20-b<1181555>, 
foi desenvolvido o ficheiro README.md na pasta CA3/part2 para descrever as etapas da tarefas desenvolvidas.
O readme desse exercício encontra-se disponível no respetivo link [CA3](https://bitbucket.org/1181555/devops-19-20-b/src/master/CA3/part2/demo/README.md). 

### Instalação e Configuração das máquinas virtuais

Nesse exercício foi feito o download do Vagrant do URL https://www.vagrantup.com/downloads.html e instalado no meu sistema operacional Windows.
Criou-se uma pasta CA3/part2 no meu repositório para fazer o download dos arquivos que estão localizados no link https://bitbucket.org/atb/vagrant-multi-spring-tut-demo/.

Foi estudado o ficheiro Vagrantfile, atualizou-se o Vagrantfile modificando o endereço do repositório que foi feito o clone e os respectivos comandos para aceder das pastas no disco local.
Realizou-se as alterações no repositório utilizado no Vagrantfile de acordo com o exemplo fornecido no link https://bitbucket.org/atb/tut-basic-gradle. Isso se deve
ao facto de ser necessário a aplicação spring usar o servidor H2 na "data base" da "virtual machine". 

Após ser feita todas alterações necessárias, procedeu-se no sistema operativo MS-DOS no diretório onde se encontra o Vagrantfile o seguinte comando:
```bash
vagrant up
```
As máquinas virtuais foram instaladas, as aplicações que estava configuradas no Vagrantfile foram instaladas e as respetivas máquinas virtuais começaram as correr normalmente.
Porém quando tentou-se aceder ao host para abrir a aplicação spring web, foi verificado um erro.

Com o comando abaixo foi possível entrar no máquina virtual web para verificar se havia sido feito o git clone do repositório:
```bash
vagrant ssh web
```

Verificou-se então que o comando git clone do Vagrantfile não foi executado, devido ao facto do repositório ser privado e requerer uma palavra-passa.
Entretanto foi executado na máquina virtual web as respectivos comandos:
```bash
git clone https://1181555@bitbucket.org/1181555/devops-19-20-b.git

cd devops-19-20-b/CA3/part2/demo

gradle clean build

sudo cp build/libs/basic-0.0.1-SNAPSHOT.war /var/lib/tomcat8/webapps
```
A execução do comando gradle clean build só foi possível após a execução do comando que da permissões ao gradle para ser executado.
Foi possível acessar a aplicação através do navegador utilizando o url http://localhost:8080/basic-0.0.1-SNAPSHOT/
e também foi possível abrir o H2 console usando o url http://localhost:8080/basic-0.0.1-SNAPSHOT/h2-console.

Por último foi feito alterações no bitbucket tornando o repositório público, as máquinas virtuais foram distruídas para que tudo funciona-se automáticamente. 
Foi feito o comando seguinte e tudo funcionou corretamente.
```bash
vagrant up
``` 


## 2. Análise da alternativa ao Virtualbox

A virtualização é um processo que permite rodar mais de um sistema operacional em uma mesma máquina. Atualmente, ela deixou de ser um recurso 
inacessível e conta com ótimas soluções voltadas para o usuário final. Uma alternativa ao VirtualBox é o VMWare Workstation. 

Um virtualizador permite instalar diferentes sistemas operacionais e rodá-los dentro de outro. Mas além desta característica, é importante que ele também possa ser utilizado em diferentes plataformas,
para que não limite sua utilização a apenas um tipo de sistema operativo. O VMWare Workstation tem versões para Windows e Linux, e no Mac a 
solução da VMWare chama-se Fusion. 

O VirtualBox ganha neste quesito, por ter suporte a uma variedade bem maior de plataformas. O programa é compativel com Windows, Mac OS X, 
Linux e Solaris, tanto para 32 como 64 bits. Além disso, no VirtualBox pode ser instalado em sistemas operacionais que seguem o padrão Unix, 
devido a similaridade de alguns com os sistemas suportados (como o Linux e o Solaris). A partir disso, conclui-se que ele oferece mais opções
para quem precisa de um sistema multiplataforma.

Ambas as soluções, por serem voltadas para usuários finais, são consideradas ferramentas fáceis de usar. 
Descontando o período de adaptação ao estilo da interface de cada um deles, a curva de aprendizagem é pequena e em pouco tempo já é possível criar,
alterar, deletar e executar máquinas virtuais nos dois programas.

O único complicador para um usuário iniciante nessas ferramentas é a própria complexidade de cada um em relação aos conceitos e recursos 
disponibilizados, o que é consequência direta de tais opções. Nesse item, o VMWare Workstation por ser uma ferramenta mais profissional, 
é de longe o mais complexo.

A versão 9 do VMWare Workstation traz uma série de novas funcionalidades ao produto e colocá-las todas em um só lugar corre o risco de tornar o 
programa irresistível. A mais visível dessas novidades é o suporte para o Windows 8 e USB 3.0; drivers gráficos melhorados, que incluem suporte a 
OpenGL para os clientes Linux, virtualização aninhada, uma série de melhorias de gestão de controle remoto e VM.

Já no VirtualBox em sua versão 4.2, o suporte a USB 2.0 é limitado, mas permite fazer diversos ajustes na configuração individual das máquinas
virtuais, aumentar ou diminuir a memória, definir quantos núcleos terá o processador, adicionar e remover discos, ativar ou desativar o acesso a
 dispositivos USB e muito mais. Para completar, é possível criar pastas compartilhadas para fazer troca de arquivos entre o computador e o novo sistema operativo.

À medida que os dois virtualizadores são avaliados em cada quesito, conclui-se que é preciso pesar muito mais que apenas os itens acima para
 decidir qual utilizar. Ambos são ótimas ferramentas de virtualização e a decisão final sobre qual utilizar depende, além dos quesitos já citados,
 da necessidade, preferência e do quanto o usuário está disposto a investir para usá-los.

## Implementação da Alternativa

A solucação alternativa do VMware Workstation seria praticamente igual ao que está descrita no implementação do Vagrantfile para o Virtualbox.
Entretanto na configuração do provider no Vagrantfile especificar o "vmware_workstation" ou apagar no Vagrantfile onde especifica "virtualbox" e quando
executar o vagrant up usar o seguinte comando:
```bash
vagrant up --provider vmware_workstation
```

# Vagrant Multi VM Demonstration for Spring Basic Tutorial Application

This project demonstrates how to setup two VMs for running the Spring Basic Tutorial application.

**web** VM:

  - Executes the web application inside Tomcat8

**db** VM:

  - Executes the H2 database as a server process. The web application connetcs to this VM.

This vagrant setup uses the spring appplication available at https://bitbucket.org/atb/tut-basic-gradle.

**Important** You should replace this version of the spring applicationn with your own. Do not forget to replicate the necessary changes in our own version! These regard essentially the support for using H2 in server mode (as opposed to memory mode).

## Requirements

  Install Vagrant and VirtualBox in you computer.

## Steps

### 1

Create a local folder in your computer and copy the Vagrantfile into that folder

### 2

Execute **vagrant up**

This will create two vagrant VMs (i.e, "db" and "web"). The first run will take some time because both machines will be provisioned with several software packages.

### 3

In the host you can open the spring web application using one of the following options:

  - http://localhost:8080/basic-0.0.1-SNAPSHOT/
  - http://192.168.33.10:8080/basic-0.0.1-SNAPSHOT/

You can also open the H2 console using one of the following urls:

- http://localhost:8080/basic-0.0.1-SNAPSHOT/h2-console
- http://192.168.33.10:8080/basic-0.0.1-SNAPSHOT/h2-console

For the connection string use: jdbc:h2:tcp://192.168.33.11:9092/./jpadb
