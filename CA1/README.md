# Relatório do Exercício Prático 01 

## 1. Análise, Design e Implementação

O respetivo exercício contempla o uso do repositório criado por cada aluno, nesse exercício foi utilizado o repositório do bitbucket criado com o nome devops-19-20-b<1181555>, 
foi copiada a pasta "src" do ficheiro disponibilizado de nome tut-react-and-spring-data-rest-mast para a pasta CA1. A fonte deste trabalho está localizada em bitbucket e em helixteamhub nos respetivos links [CA1](https://bitbucket.org/1181555/devops-19-20-b/src/master/CA1/) e
[CA1](https://helixteamhub.cloud/big-arrow-6368/projects/devops-19-20-b-_1181555_/repositories/devops-19-20-b-1181555/tree/default).

### Adicionando tag

O primeiro passo do exercício consistiu em adicionar uma tag ao master branch já existente. Isto foi feito com o uso do comando git:

```git
git tag -a v1.2.0 -m "My version v1.2.0" dd6ed39
```

Onde a opção -a informa ao git que a tag é uma tag anotada e não uma tag leve, o que significa que é uma soma de verificação e é armazenada como um objeto completo no banco de dados Git, e a opção -m fornece ao git uma mensagem para acompanhar a tag.
Os números após a mensagem correspondem ao número do commit ao qual queremos adicionar a tag.

Os pushs normais não enviam tags para o repositório - portanto, a tag precisa ser enviada individualmente com o comando:

```git
git push origin v1.2.0
```

### Criando o branch

Posteriormente, um branch com o nome email-field foi criado para adicionar o suporte de email para a aplicação em desenvolvimento.
O branch foi criado e ativado usando os comandos: 

```git
git branck email-field
git checkout email-field
```

O comando git push exigia que um upstream fosse definido para o branch, por meio do git push --set-upstream origin email-field.


### Adicionando suporte para novas características

Os respetivos commits foram efetuados utilizando o branch email-field e fix-invalid-email.

O atributo e o suporte foram adicionados ao campo de email para ser introduzido na lista de informações do funcionário. Isto foi realizado introduzindo o campo do email em determinadas classes, nomeadamente Employee e Databaseloader. E as mudanças necessárias foram realizadas no app.js

A classe Employee teve o maior número de alterações, com a introdução do atributo de classe chamado descrição e email, armazenado esses atributos como uma String, e a criação de métodos getter / setter para esses atributos, assim como um método para validar se o email é valido e contém o @. As alterações foram confirmadas no repositório com o uso do git commit -m, seguido pelo git push.

## Testando a nova implementação

Para testar o novo recurso, a primeira alteração foi atualizar a dependência do Maven, para que ele usasse a versão mais atual do JUnit. Isso foi feito atualizando o arquivo pom.xml e reimportando as dependências do Maven.

Após essa alteração, uma nova classe foi criada e adicionada à pasta de teste, chamada EmployeeTest. Lá, os métodos da classe Employee.java receberam testes unitários para null, empty e validar se contém @ de acordo com a metodologia da JUnit, classificados pelo método Arrange / Act / Assert e utilizando AssertEquals e  assertThrows.

Mais uma vez, as alterações foram enviadas ao repositório usando o git commit -m seguidos pelo git push. Mas, desta vez, novos arquivos foram adicionados, ou seja, EmployeeTest.java, portanto, antes do git commit -m, precisávamos garantir que todos os arquivos fossem rastreados. 

Para fazer isso, navegamos até a pasta CA1 usando o comando cd e, em seguida, usamos git add. para rastrear todos os arquivos na pasta. Após esse comando, o git commit -m e o git push foram suficientes para colocar a nova classe de teste no controle de versão.


### Gitignore

No começo do trabalho foi adicionado ao repositório um arquivo “.gitignore“ para evitar que em tempo de execução arquivos supérfluos fossem rastreados pelo controle de versão. 

Dessa forma, criamos um arquivo .gitignore. Depois disso, o git add . .gitignore, seguido pelo git commit -m e git push, garantiu que o arquivo gitignore chegasse ao repositório.

### Merge

Posteriormente, cada recurso que foi implementado e testado, decidimos mesclá-los com a branch principal. Fizemos isso a partir da branch do campo email-field e fix-invalid-email, usando:
```git
git checkout master
git merge email-field
git push origin

git checkout master
git merge fix-invalid-email
git push origin

```

Isso garantiu que o master estivesse atualizado com o campo email e nos fez voltar a trabalhar no ramo master, quando que o branch do campo email-field não era mais necessário ele foi apagado. Foi criado o branch fix-invalid-email para resolver um novo problema que foi aberto.

Como é possível ver anteriormente após a resolução do problema, o branch fix-invalid-email, sofreu o merge com o master e de seguida apagado.

### Apagando tags
```git
git branck -d email-field
git branck -d fix-invalid-email
```

### Adicionando nova tag

Depois que a funcionalidade foi implementada, uma nova tag foi adicionada, desta vez v1.3.0. Foi usado:

```git
git tag -a v1.3.0 -m "Adicionar email funcionalidade e tests v1.3.0" 
git push origin v1.3.0
```

Posteriormente, foi criada outra tag para implementar uma solução para que o campo de email seja preenchido com um email válido e por fim uma tag para assinalar o final do exercício.

```git
git tag -a v1.3.1 -m "Adicionar validação de email válido v1.3.1" 
git push origin v1.3.1
```

```git
git tag -a ca1 -m "Sinalizar o final do exercício" 
git push origin ca1
```

### Consertando bug e realizando validações

Para adicionar validações e corrigir bugs, um novo branch foi criado, chamado fix-email-valid, com validações de branch git, seguidas pelas validações de checkout do git.

As validações foram adicionadas aos principais métodos construtores da classe Employee.java e os testes foram adicionados à classe EmployeeTest.java.

Essas mudanças foram confirmadas pelo git commit -m, seguido pelo git push. O comando git push exigia que um upstream fosse definido para o branch, por meio de validações de origem git push --set-upstream.

## 2. Análise do alternativa ao Git

#### MERCURIAL

O Mercurial é outro opção de controle de versão distribuído assim como o Git, ele foi desenvolvido para projetos de grande porte, muito além dos projetos simples de designers e desenvolvedores independentes, obviamente isso não significa que times pequenos não possam utilizá-lo, o Mercurial é extremamente rápido e os criadores focaram na performance como recurso mais importante. 
Além de ser muito rápido e escalável, o Mercurial é mais simples que o Git, não existem tantas funções para aprender e as funções são similares a outros controlos de versão, além de ele vir equipado com uma interface web e ter uma excelente documentação. 

### Complexidade

A complexidade da ferramenta tem impacto direto na produtividade dos desenvolvedores e é o fator decisivo para o aprendizado e operação correto da ferramenta. Os critérios usados para medi-la foram o número de comandos e número de linhas dos textos de ajuda. 
Embora não sejam perfeitos, são suficientes para apontar indícios a respeito da complexidade da ferramenta, além de serem imparciais e de fácil medição. Outras dimensões de complexidade das ferramentas serão abordadas em análises posteriores. 
O detalhamento dos critérios e das premissas de comparação são apresentados a seguir:

Número de comandos. Todas as ferramentas possuem dezenas comandos e conhecê-los é um desafio para qualquer usuário.
Premissa: Quanto menor o número de comandos, mais fácil de conhecê-los.

Números de linhas dos textos de ajuda. Todas as ferramentas textos de ajuda para seus comandos com objetivo de apresentar o funcionamento do comando e as opções disponíveis. Premissas:

a. O formato e a qualidade dos textos de ajuda das diferentes ferramentas sendo analisadas são equivalentes.

b. Os textos de ajuda contêm apenas o mínimo de conteúdo necessário para descrever adequadamente os comandos.

c. Comandos mais complexos precisam de textos mais longos para serem descritos. Portanto, quanto menor o número de linhas do texto de ajuda necessário para explicar um comando, mais simples ele é.

O Mercurial possui 55 comandos no total (com fetch e rebase habilitados), podendo aumentar de acordo com o número de extensões habilitadas. Dezoito desses comandos são considerados básicos, apresentados ao se executar o comando hg sem nenhum subcomando ou parâmetro adicional.

## 3. Implementação da Alternativa 

A implementação alternativa foi realizado utilizando o Mercurial, como referido anteriormente. Os comandos são muito parecidos com o git e a exemplo da solução e implementação utilizada no primeiro capítulo serão apresentados abaixo os comandos necessários:

Foi criado um novo projeto com mesmo nome que tinha no repositório Git.
Na pasta do novo repositório foi realizado os respectivos comandos para iniciar repositório, posteriormente adicionar a cópia dos ficheiros, commit e push dos ficheiros para o servidor da helixteamhub,  

### Iniciar repositório 
```hg
hg init 
hg add .
hg commit -m 'Initial commit'
hg push 
````

Os seguintes comandos foram utilizados para criar as respetivas tags.

### Adicionando tag
```hg
hg tag -e v1.2.0 "My version v1.2.0" 
hg tag -e v1.2.0 "My version v1.3.0"
hg tag -e v1.2.0 "My version v1.3.1"
hg tag -e CA1 "My version CA1"   
````
Para ver todas as tags podemos utilizar o comando "hg tags".
Os comando a seguir servem para criar, ativar e fazer merge dos respetivos branches.

### Criando o branch
```hg
hg branch email-field
hg update default
hg merge email-field

hg branch fix-invalid-email
hg update default
hg merge fix-invalid-email
````

Para ver os branches ativos utiliza-se os comando hg branches. Para marcar um branch como inativo, por exemplo, porque você concluiu a implementação do recurso, você pode fechá-lo.
```hg
$ hg update fix-invalid-email
$ hg commit --close-branch -m "finished fix-invalid-email"
````

Todos os outros aspetos relativos a implementação da solução alternativa são iguais e já foram explicados anteriormente.

## Conclusão

O Mecurial têm comandos equivalentes ao Git. Pela lógica utilizada, os comandos do Mercurial são mais simples dos que os do Git.
O Git possui textos mais longos em todos os casos, o que indica seus comandos são mais difíceis de serem explicados.
Porém o Git aparentemente é a ferramente de versão de controlo preferida e mais usada a nível mundial. 