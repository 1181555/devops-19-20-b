# Relatório do Exercício Prático CA5 - parte 1

## 1. Análise, Design e Implementação

O respetivo exercício contempla o uso do repositório criado por cada aluno, nesse exercício foi utilizado o repositório do bitbucket criado com o nome devops-19-20-b<1181555>, 
foi desenvolvido o ficheiro README.md na pasta CA5 part 1 para descrever as etapas das tarefas desenvolvidas.
O readme desse exercício encontra-se disponível no respetivo link [CA5](https://bitbucket.org/1181555/devops-19-20-b/src/master/CA5/part1/README.md). 

### Instalação e Configuração do Jenkins

Nesse exercício foi feito o download do ficheiro jenkins.war do URL https://www.jenkins.io/doc/book/installing/ e instalado no meu sistema operativo Windows através da linha de comando na pasta onde encontrava-se o respetivo ficheiro com o seguinte comando:

```jenkins
java -jar jenkins.war
```

Posteriormente no browser através do endereço `localhost:8080` foi possível fazer a configuração do Jenkins selecionando a opção de instalação de plugins sugeridos, foi definido o nome
 de utilizador e a palavara-passe, assim como o email de Administrador e por fim foi possível utilizar a aplicação.


### Criação do Jenkinsfile

Na pasta do repositório onde encontra-se o exercício do Gradle Basic Demo foi adicionado o ficheiro Jenkinsfile e depois foi feito o push para o Bitbucket.
O ficheiro Jenkinsfile contém o seguinte script de pipeline com stages para Checkout, Assemble, Test e Archive:

```jenkins
 pipeline {
             agent any

         	stages {
         	    stage('Checkout') {
         	        steps {
         	            echo 'Checking out...'
         	            git credentialsId: 'tarcisio-bitbucket', url:'https://1181555@bitbucket.org/1181555/devops-19-20-b'
         	        }
         	    }
         	    stage('Assemble') {
         	        steps {
         	            echo 'Assembling...'
         	            bat 'cd CA2/part1 & gradlew clean assemble'
         	        }
         	    }
         	    stage('Test') {
         	        steps {
         	            echo 'Testing...'
         	            bat 'cd CA2/part1 & gradlew test'
         	            junit 'CA2/part1/build/test-results/test/*.xml'

         	        }
         	    }
         	    stage('Archiving') {
         	        steps {
         	            echo 'Archiving...'
         	            archiveArtifacts 'CA2/part1/build/distributions/*'
         	        }
         	    }
         	}
         }
```



### Criação do Pipeline Job

Na aplicação do Jenkins foi criado um novo pipeline usando o `New Item` no menu principal e o mesmo foi nomeado como CA5-part1.
Na parte de definições de pipeline, foi selecionado Pipeline script from SCM. Definiu-se que o SCM deveria ser o Git, foi adicionado o URL do meu repositório do Bitbucket e definiu-se as credenciais 
do Bitbucket que foi anteriormente configurada.

Na parte do caminho do Script foi definido o caminho para o Jenkinsfile no repositório (CA2/part1/Jenkinsfile), selecionou-se aplicar e guardar as alterações. 

Após a realização das etapas anteriores foi selecionada na barra de opções da pipeline CA5-part1 a opção `Build now` onde construiu com sucesso todas as etapas do pipeline.

Foi possível verificar que no `Test Result` ele listou o resultado dos testes realizados.

	