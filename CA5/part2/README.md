# Relatório do Exercício Prático CA5 - parte 2

## 1. Análise, Design e Implementação

O respetivo exercício contempla o uso do repositório criado por cada aluno, nesse exercício foi utilizado o repositório do bitbucket criado com o nome devops-19-20-b<1181555>, 
foi desenvolvido o ficheiro README.md na pasta CA5 part2 para descrever as etapas da tarefas desenvolvidas.
O readme desse exercício encontra-se disponível no respetivo link [CA5](https://bitbucket.org/1181555/devops-19-20-b/src/master/CA5/part2/README.md). 

### Instalação e Configuração das máquinas virtuais

Nesse exercício foi executado através da linha de comando na pasta onde encontrava-se o respetivo ficheiro jenkins.war o seguinte comando para iniciar a aplicação Jenkins:
```jenkins                                                                                                                                                                         
java -jar jenkins.war                                                                                                                                                                           
```

### Criação do Jenkinsfile

Na pasta do repositório onde encontra-se o exercício do Gradle Demo Parte 2 foi adicionado o ficheiro Jenkinsfile e depois foi feito o push para o Bitbucket.
O ficheiro Jenkinsfile contém o seguinte script de pipeline com stages para Checkout, Assemble, Test, Javadoc, Archive e Publish Image:
```jenkins                                                                                                                                                                         
 pipeline {
             agent any

         	stages {
         	    stage('Checkout') {
         	        steps {
         	            echo 'Checking out...'
         	            git credentialsId: 'tarcisio-bitbucket', url:'https://1181555@bitbucket.org/1181555/devops-19-20-b'
         	        }
         	    }
         	    stage('Assemble') {
         	        steps {
         	            echo 'Assembling...'
         	            bat 'cd CA2/part2/demo & gradlew clean assemble'
         	        }
         	    }
         	    stage('Test') {
         	        steps {
         	            echo 'Testing...'
         	            bat 'cd CA2/part2/demo & gradlew test'
         	            junit 'CA2/part2/demo/build/test-results/test/*.xml'

         	        }
         	    }
         	    stage('Javadoc') {
                    steps {
                        echo 'Generating Javadoc...'
                        bat 'cd CA2/part2/demo & gradlew javadoc'
                        publishHTML([allowMissing: false, alwaysLinkToLastBuild: false, keepAll: false, reportDir: 'CA2/part2/demo/build/docs/javadoc', reportFiles: 'index.html', reportName: 'HTML Report', reportTitles: ''])
                    }
                }
         	    stage('Archiving') {
         	        steps {
         	            echo 'Archiving...'
         	            archiveArtifacts 'CA2/part2/demo/build/libs/*'
         	        }
         	    }
         	    stage('Pubish Image') {
                    steps {
                        echo 'Publishing docker image...'
                        script{
                            docker.build("1181555/devops_ca5_part2:${env.BUILD_ID}", "CA2/part2/demo").push()
                        }
                    }
                }
         	}
  }                                                                                                                                                                        
```

Para executar a última etapa do pipeline, um Dockerfile foi incluído no repositório ca2-part2, cujo conteúdo é
idêntico ao Dockerfile `web` do exercício ca4. Esse estágio faz a construção de um contentor e posteriormente o upload da imagem para o DockerHub com o prefixo `devops_ca5_part2` associado
a tag com o ID da construção do contentor. 

### Criação e execução do Pipeline Job

Na aplicação do Jenkins foi criado um novo pipeline usando o `New Item` no menu principal e o mesmo foi nomeado como CA5-part2.
Na parte de definições de pipeline, foi selecionado Pipeline script from SCM. Definiu-se que o SCM deveria ser o Git, foi adicionado o URL do meu repositório do Bitbucket e definiu-se as credenciais 
do Bitbucket que foi anteriormente configurada.

Na parte do caminho do Script foi definido o caminho para o Jenkinsfile no repositório (CA2/part2/Jenkinsfile), selecionou-se aplicar e guardar as alterações. 

Após a realização das etapas anteriores foi selecionada na barra de opções da pipeline CA5-part2 a opção `Build now` onde construiu com sucesso todas as etapas do pipeline.


## 2. Análise da alternativa ao Jenkins

A solução alternativa analisada é o `Buddy` que é capaz de construir, testar e implementar com um push em segundos. 
É uma plataforma com integração para diversos repositório, com ferramentas baseadas em Docker e de implementação contínua.

O Buddy pertence à categoria "Implementação contínua", enquanto o Jenkins pode ser classificado principalmente em "Integração contínua".

Alguns dos recursos oferecidos pelo Buddy são:

- Implementações automáticas ao enviar para o branch 
- Construções e testes baseados no Docker
- Configuração de 10 minutos do ambiente completo

Por outro lado, o Jenkins fornece os seguintes recursos principais:
- Instalação fácil
- Configuração fácil
- Alterar conjunto de suporte

O Buddy tem uma interface de utilizador muito intuitiva. Suas integrações vêm com padrões 
sensíveis e instruções claras. Sem a carga de manutenção e instalação, o único requisito de aprendizado é 
trabalhar com a própria ferramenta. Isso não requer nenhum conhecimento profundo dos internos da Buddy.

## 3. Implementação da Alternativa

Para a implementação alternativa o primeiro passo foi criar uma conta Buddy, associando à minha conta do BitBucket para acessar o meu repositório,
posteriormente um novo pipeline com a denominação ca5-part2 foi criado.

A semelhança do que foi feito no Jenkins, foram criados as etapas Checkout, Assemble, Test, Javadoc, Archive e Publish Image,
Na parte do `Actions` uma nova ação do tipo `BUILD TOOLS & TASK RUNNERS` e o subtipo `GRADLE` foi adicionada.
Na parte do `RUN`, foi incluído os seguintes comandos, no environment foi feito comandos para atualização e no cache foi selecionada
a pasta do repositório CA2/part2/demo:

```buddy                                                                                                                                                                       
chmod u+x ./gradlew

./gradlew clean assemble                                                                                                                                                                         
```


Para replicar o estágio `Test`, a exemplo da etapa anterior foi selecionada uma nova uma nova ação do tipo `BUILD TOOLS & TASK RUNNERS` e o subtipo `GRADLE`,
na parte do `RUN`, foi incluído os seguintes comandos, no environment foi feito comandos para atualização e no cache foi selecionada a pasta do repositório CA2/part2/demo:

```buddy                                                                                                                                                                        
./gradlew test                                                                                                                                                                        
```
  
Para replicar o estágio `Javadoc`, a exemplo da etapa anterior foi selecionada uma nova uma nova ação do tipo `BUILD TOOLS & TASK RUNNERS` e o subtipo `GRADLE`,
na parte do `RUN`, foi incluído os seguintes comandos, no environment foi feito comandos para atualização e no cache foi selecionada a pasta do repositório CA2/part2/demo:

```buddy                                                                                                                                                                        
./gradlew javadoc                                                                                                                                                                       
```

Para replicar o estágio `Archive`, foi selecionada uma nova uma nova ação do tipo `Devops` e o subtipo `Zip`,
na parte do `Setup`, foi fornecido o caminho da pasta de onde e para onde devem ser arquivados os ficheiros, assim como o nome final do ficheiro.  

Para replicar o estágio `Build Docker`, foi selecionada uma nova uma nova ação do tipo `Docker` e o subtipo `Build Image`,
na parte do `Setup`, foi fornecido o caminho do repositório onde se encontra o ficheiro Dockerfile,  na parte do `Option` foi 
feito a configuração para login no Docker Hub, nome do repositório e no da tag.


E por fim para fazer o push da imagem, foi selecionada uma nova uma nova ação do tipo `Docker` e o subtipo `Push Docker Image`,
na parte do `Setup`, foi definido que deveria ser feito push da image construída anteriormente e foi 
feito a configuração para login no Docker Hub, nome do repositório e no da tag.

Após a configuração de todos esses estágios Buddy, foi executado o pipeline. Todas os estágios foram efectuados com sucesso.